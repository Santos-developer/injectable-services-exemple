import { HttpServiceAbstract } from "./http-service-abstract";

interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export class PostsServices extends HttpServiceAbstract {
  public async getAll() {
    return await this.run<Post[]>(
      this.get("https://jsonplaceholder.typicode.com/posts")
    );
  }
}
