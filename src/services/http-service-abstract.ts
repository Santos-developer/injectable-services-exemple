import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { Dispatch } from "redux";
import { Feedback, FeedbackParams } from "../hooks/useFeedback";
import { Loading } from "../hooks/useLoading";

export type SuccessFeedback<Data> = (
  response: AxiosResponse<Data>,
  dispatch: Dispatch
) => FeedbackParams;

export type ErrorFeedback = (
  reason: Error,
  dispatch: Dispatch
) => FeedbackParams;

export interface FeedbacksParams<Data> {
  success: SuccessFeedback<Data>;
  error: ErrorFeedback;
}

export interface SimplesApiRequestConfig extends AxiosRequestConfig {
  withAuthorization?: boolean;
}

export abstract class HttpServiceAbstract {
  protected loading!: Loading;
  protected feedback!: Feedback;
  protected dispatch!: Dispatch;

  constructor(private instance = axios.create()) {}

  protected post<RESPONSE, REQUEST>(
    url: string,
    request?: REQUEST,
    params: SimplesApiRequestConfig = {}
  ): Promise<RESPONSE> {
    return this.instance
      .post<RESPONSE>(url, request, params)
      .then((response) => response?.data);
  }

  protected put<RESPONSE, REQUEST>(
    url: string,
    request: REQUEST,
    params: SimplesApiRequestConfig = { withAuthorization: false }
  ): Promise<RESPONSE> {
    return this.instance
      .put<RESPONSE>(url, request, params)
      .then((response) => response?.data);
  }

  protected patch<RESPONSE, REQUEST>(
    url: string,
    request: REQUEST,
    params: SimplesApiRequestConfig = { withAuthorization: false }
  ): Promise<RESPONSE> {
    return this.instance
      .patch<RESPONSE>(url, request, params)
      .then((response) => response?.data);
  }

  protected get<RESPONSE>(
    url: string,
    params: SimplesApiRequestConfig = { withAuthorization: false }
  ): Promise<RESPONSE> {
    return axios.get<RESPONSE>(url, params).then((response) => response?.data);
  }

  protected delete(
    url: string,
    params: SimplesApiRequestConfig = { withAuthorization: false }
  ): Promise<unknown> {
    return axios.delete(url, params).then((response) => response?.data);
  }

  protected async run<Data>(
    handler: Promise<AxiosResponse<Data>>,
    feedbacks?: FeedbacksParams<Data>
  ) {
    try {
      this.loading.show();

      const result = await handler;

      if (feedbacks) {
        this.feedback.show(feedbacks.success(result, this.dispatch));
      }

      return { result, dispatch: this.dispatch };
    } catch (e) {
      if (feedbacks) {
        this.feedback.show(feedbacks.error(e, this.dispatch));
      }

      return { reason: e, dispatch: this.dispatch };
    } finally {
      this.loading.hide();
    }
  }
}
