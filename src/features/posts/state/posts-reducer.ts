export interface Post {
  id: number;
  userId: number;
  title: string;
  body: string;
}

interface InitialState {
  posts: Post[];
}

interface BaseAction {
  type: string;
}

interface ActionSetPosts extends BaseAction {
  posts: Post[];
}

type Action = ActionSetPosts;

const INITIAL_STATE: InitialState = {
  posts: [],
};

const postsReducer = (state = INITIAL_STATE, action: Action): InitialState => {
  switch (action.type) {
    case "SET_POSTS":
      return { ...state, posts: action.posts };
    default:
      return state;
  }
};

export default postsReducer;
