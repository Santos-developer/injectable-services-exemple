import { Post } from "./posts-reducer";

export const SET_POSTS = (posts: Post[]) => ({
  type: "SET_POSTS",
  posts,
});
