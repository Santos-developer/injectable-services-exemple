/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback } from "react";
import useService from "../../hooks/useService";
import { PostsServices } from "../../services/posts-services";
import { useTypedSelector } from "../../store";

const ExamplePage = () => {
  const posts = useTypedSelector((state) => state.posts.posts);
  const postsServices = useService<PostsServices>(PostsServices);

  const onClick = useCallback(() => {
    postsServices.getAll().then(({ result, dispatch }) => {
      dispatch({ type: "SET_POSTS", posts: result });
    });
  }, [postsServices]);

  return (
    <>
      <button onClick={onClick}>Buscar posts</button>
      <p style={{ color: "#ffffff" }}>{JSON.stringify(posts)}</p>
    </>
  );
};

export default ExamplePage;
