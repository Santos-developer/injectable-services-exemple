import React from "react";
import { Provider } from "react-redux";
import { createStore } from "redux";
import ExamplePage from "./features/posts/posts";
import RootReducer from "./store";

const store = createStore(RootReducer);

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <ExamplePage />
      </div>
    </Provider>
  );
}

export default App;
