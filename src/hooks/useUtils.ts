import { useDispatch } from "react-redux";
import useFeedback from "./useFeedback";
import useLoading from "./useLoading";

const useUtils = () => {
  const loading = useLoading();
  const feedback = useFeedback();
  const dispatch = useDispatch();

  return { loading, feedback, dispatch };
};

export default useUtils;
