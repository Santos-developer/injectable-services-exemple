import { useCallback } from "react";

export interface FeedbackParams {
  title: string;
  message: string;
  component: "MODAL" | "ALERT";
}

export interface Feedback {
  show: (params: FeedbackParams) => void;
  hide: (params: FeedbackParams) => void;
}

const useFeedback = (): Feedback => {
  const show = useCallback((params: FeedbackParams) => {
    alert(JSON.stringify(params));
  }, []);

  const hide = useCallback((params: FeedbackParams) => {
    alert(JSON.stringify(params));
  }, []);

  return { show, hide };
};

export default useFeedback;
