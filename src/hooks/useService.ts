import { useMemo } from "react";
import useUtils from "./useUtils";

type ServiceClass<Service> = new () => Service;

const useService = <Service>(ServiceClass: ServiceClass<Service>): Service => {
  const utils = useUtils();

  const service = useMemo(() => {
    const instance = new ServiceClass();
    Object.assign(instance, utils);
    return instance;
  }, [ServiceClass, utils]);

  return service;
};

export default useService;
