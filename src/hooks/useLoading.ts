import { useCallback } from "react";

export interface Loading {
  show: () => void;
  hide: () => void;
}

const useLoading = (): Loading => {
  const show = useCallback(() => {
    alert("Loading aberto");
  }, []);

  const hide = useCallback(() => {
    alert("Loading fechado.");
  }, []);

  return { show, hide };
};

export default useLoading;
