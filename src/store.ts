import { combineReducers } from "redux";
import { TypedUseSelectorHook, useSelector } from "react-redux";
import postsReducer from "./features/posts/state/posts-reducer";

const RootReducer = combineReducers({
  posts: postsReducer,
});

export type RootState = ReturnType<typeof RootReducer>;
export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;
export default RootReducer;

export interface DefaultAction {
  type: string;
}
